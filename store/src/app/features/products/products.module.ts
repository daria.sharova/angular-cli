import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { ProductComponent } from './product/product.component';

import { ButtonModule } from 'src/app/shared/components/button/button.module';
import { InputModule } from 'src/app/shared/components/input/input.module';


@NgModule({
  declarations: [
    ProductsComponent,
    ProductComponent    
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ButtonModule,
    InputModule,
    
  ],
  exports: [ProductsComponent, ProductComponent]
})
export class ProductsModule { }
