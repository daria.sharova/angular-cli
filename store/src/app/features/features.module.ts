import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsModule } from './products/products.module';
import { CartModule } from './cart/cart.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  exports: [ProductsModule, CartModule]
})
export class FeaturesModule { }
